# polld

polld - commandline application to hold a p2p election between several options.

## Polling Procedure
The poll is created by creating a manifest and defining
the set of public keys of the voters.

Voters can then create their votes on ipfs, sign them and
publish the CID of their vote on IPFS PubSub for that poll.

This means:
1. The elections are not secret. Anyone can read the votes and know who voted how.
2. The elections are verifiable. Anyone can verify and certify an election.
3. 