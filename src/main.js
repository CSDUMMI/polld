const IPFS = require("ipfs")
const IPFSHttp = require("ipfs-http-client")
const {
  signCID,
  verifySignature,
  createPublicKey,
  loadKeys,
  generateKeyPair
} = require("./crypto")
const yargs = require("yargs")

const encoder = new TextEncoder()
const decoder = new TextDecoder()


/**
 * Create a polld Manifest on IPFS containing the name, options
 * and the public keys of the voters.
 * @param name {string} name of the pubsub channel of the poll.
 * @param options {Array<string>} to vote on.
 * @param keys {Array<string>} public keys of the voters.
 */
function createPoll(ipfs, name, options, keys) {
  return ipfs.dag.put({
    polld: "manifest",
    name: name,
    options: options,
    keys: keys,
  }, {
    format: 'dag-cbor',
    hashAlg: 'sha2-512'
  })
}

/**
 * votePoll - vote in a poll that you're public key is added to.
 */
async function votePoll(ipfs, cid, vote, publicKey, privateKey) {
  const voteCID = await ipfs.dag.put({
    polld: "vote",
    poll: cid,
    vote: vote,
  }, {
    format: 'dag-cbor',
    hashAlg: 'sha2-512'
  })

  const signature = signCID(voteCID, privateKey)

  const pubsubObjCID = await ipfs.dag.put({
    polld: "pubsubObj",
    cid: voteCID,
    signature: signature,
    publicKey: publicKey
  })

  const pollManifest = (await ipfs.dag.get(cid)).value

  const channel = "polld-" + pollManifest.name

  let msg = "vote" + pubsubObjCID.toString()

  await ipfs.pubsub.publish(channel, encoder.encode(msg))

  return pubsubObjCID
}

/**
 * wait on a poll to finish, count the votes
 * until one option receives 50% of the votes.
 * Then return the elections result.
 */
async function waitOnPoll(ipfs, manifestCID, onResult) {
  const pollManifest = (await ipfs.dag.get(manifestCID)).value


  const channel = "polld-" + pollManifest.name


  const handler = async msg => {
    const data = decoder.decode(msg.data)

    // vote pubsub message
    if (data.startsWith("vote")) {
      const votes = {}
      const hasVoted = new Set()

      pollManifest.options.forEach(option => {
        votes[option] = []
      })

      const pubsubCID = IPFS.CID.parse(data.substring("vote".length))

      const pubsubObj = (await ipfs.dag.get(pubsubCID)).value

      verifyVote(pubsubObj, pollManifest.keys, manifestCID, hasVoted)

      votes[vote.vote].push(pubsubObj)
      hasVoted.add(publicKey)

      let result
      if (result = checkPollResult(votes, hasVoted)) {
        const certificate = await certifyPollResult(ipfs, channel, result, manifestCID)

        ipfs.pubsub.unsubscribe(channel, handler)
        onResult(certificate)
      }

      // certify pubsub message
    } else if (data.startsWith("certify")) {
      const resultCID = IPFS.CID.parse(data.substring("certify".length))

      const certificate = (await ipfs.dag.get(resultCID)).value

      if (certificate.manifest !== manifestCID) {
        return
      }

      const votes = certificate.result.votes


      const hasVoted = new Set()

      // Verify all votes are valid
      for (let vote of votes) {

        if (await verifyVote(vote, pollManifest.keys, manifestCID, hasVoted) &&
          vote.cid == manifestCID &&
          vote.vote == result.winner) {
          hasVoted.add(vote.publicKey)
        } else {
          return
        }
      }

      // Verify the result is valid
      if (votes.length > pollManifest.keys.length / 2) {
        onResult(certificate)
      }
    }
  }

  await ipfs.pubsub.subscribe(channel, handler)
}

function checkPollResult(votes, hasVoted, votersNum) {
  let max = 0
  let winner = null;

  for (let option of Object.keys(votes)) {
    if (votes[option].length > votersNum / 2) {
      return {
        winner: option,
        votes: votes,
      }
    }
  }

  return null
}

async function certifyPollResult(ipfs, channel, result, manifestCID) {
  const certificate = {
    polld: "certificate",
    result: result,
    manifest: manifestCID,
  }

  const resultCID = await ipfs.dag.put(certificate, {
    format: 'dag-cbor',
    hashAlg: 'sha2-512'
  })

  await ipfs.pubsub.publish(channel, "certify" + resultCID.toString())
  return certificate
}


async function verifyVote(pubsubObj, keys, manifestCID, hasVoted) {
  const voteCID = pubsubObj.cid
  const signature = pubsubObj.signature
  const publicKey = pubsubObj.publicKey

  if (hasVoted.has(publicKey)) {
    return
  }

  if (!crypto.verifySignature(voteCID, signature, publicKey)) {
    return
  }

  if (!pollManifest.keys.includes(publicKey)) {
    return
  }

  const vote = (await ipfs.dag.get(voteCID)).value

  if (vote.polld != "vote" || vote.poll != manifestCID) {
    return
  }

  return true
}

/**
 * You can create a poll, vote in a poll and
 * wait on a poll.
 */
async function main() {
  let ipfs

  yargs
    .scriptName("polld")
    .usage("$0 <cmd> [args]")
    .option("private", {
      alias: "priv",
      describe: "private key file",
      default: "privkey.pem",
    })
    .option("public", {
      alias: "pub",
      describe: "public key file",
      default: "pubkey.pem",
    })
    .option("ipfs-repo", {
      alias: "ipfs",
      describe: "ipfs repo location",
      default: "./jsipfs",
    })
    .option("node", {
      alias: "n",
      describe: "run a full ipfs node",
      default: false,
      type: "boolean",
    })
    .command("create", "Create a poll", args => {
      args.positional("name", {
        type: "string",
        default: "examplepoll",
        describe: "The name of the poll"
      })

      args.option("options", {
        type: "string",
        array: true,
        default: ["a", "b", "c"],
        describe: "options to poll between",
      })

      args.option("participants", {
        type: "string",
        array: true,
        alias: "keys",
        describe: "public key CIDs of the participants",
      })
    }, async argv => {
      const {
        publicKey,
        privateKey
      } = loadKeys(argv.public, argv.private)

      let publicKeys = []
      for (let keyCID of argv.participants) {
        const cid = IPFS.CID.parse(keyCID)
        const publicKey = await ipfs.dag.get(cid)
        publicKeys.push(createPublicKey(publicKey.value.publicKey))
      }

      console.log((await createPoll(ipfs, argv.name, argv.options, argv.participants)).toString())
      process.exit(0)
    })
    .command("generate", "Generate key pair", args => {
      args.positional("publicKeyFilename", {
        type: "string",
        default: "pubkey.pem",
        description: "file to write public key to",
      })

      args.positional("privateKeyFilename", {
        type: "string",
        default: "privkey.pem",
        description: "file to wirte private key to",
      })
    }, async argv => {
      const {
        publicKey,
        privateKey
      } = generateKeyPair(argv.publicKeyFilename, argv.privateKeyFilename)

      const cid = await ipfs.dag.put({
        publicKey: publicKey.export({
          format: "pem",
          type: "spki",
        }, {
          format: 'dag-cbor',
          hashAlg: 'sha2-512'
        })
      })

      console.log(cid.toString())
      process.exit(0)
    })
    .command("vote", "Vote on a poll", args => {
      args.option("poll", {
        type: "string",
        default: "",
        description: "poll cid",
      })

      args.option("vote", {
        alias: "v",
        type: "string",
        default: "",
        description: "option you want to vote for"
      })
    }, async argv => {
      const {
        publicKey,
        privateKey
      } = loadKeys(argv.public, argv.private)

      const pubsubCID = await votePoll(ipfs, IPFS.CID.parse(argv.poll), argv.vote, publicKey, privateKey)
      console.log(pubsubCID.toString())
      process.exit(0)
    })
    .command("wait", "wait on a poll to complete", args => {
      args.option("poll", {
        type: "string",
        default: "",
        description: "poll cid",
      })

      args.option("timeout", {
        type: "number",
        default: null,
        describe: "set a timeout to the execution of the command",
      })
    }, async argv => {
      if (argv.timeout) {
        setTimeout(() => {
          process.exit(1)
        }, argv.timeout)
      }

      await waitOnPoll(ipfs, IPFS.CID.parse(argv.poll), certificate => {
        console.log(certificate)
        process.exit(0)
      })
    })
    .middleware(async argv => {
      if (argv.node) {
        ipfs = await IPFS.create({
          silent: true,
          repo: argv.ipfsRepo,
        })
      } else {
        ipfs = IPFSHttp.create()
      }
    })
    .help()
    .argv
}

main()