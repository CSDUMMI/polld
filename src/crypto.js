const crypto = require("crypto")
const fs = require("fs")

function signCID(cid, privateKey, hashAlgo = "SHA256") {
  const sign = crypto.createSign(hashAlgo)
  sign.write(cid.toString())
  sign.end()

  return sign.sign(privateKey, "base64url")
}

function verifySignature(cid, signature, publicKey, hashAlgo = "SHA256") {
  const verify = crypto.createVerify(hashAlgo)
  verify.write(cid)
  verify.end()
  return verify.verify(publicKey, signature, "base64url")
}

function loadKeys(publicKeyFilename, privateKeyFilename) {
  const publicKey = crypto.createPublicKey(fs.readFileSync(publicKeyFilename))
  const privateKey = crypto.createPrivateKey(fs.readFileSync(privateKeyFilename))
  return {
    publicKey,
    privateKey
  }
}

function generateKeyPair(publicKeyFilename, privateKeyFilename) {
  const {
    publicKey,
    privateKey
  } = crypto.generateKeyPairSync("rsa", {
    modulusLength: 4096,
  })

  fs.writeFileSync(publicKeyFilename, publicKey.export({
    type: "spki",
    format: "pem",
  }))

  fs.writeFileSync(privateKeyFilename, privateKey.export({
    type: "pkcs1",
    format: "pem",
  }))

  return {
    publicKey,
    privateKey,
  }
}


const createPublicKey = crypto.createPublicKey

module.exports = {
  signCID,
  verifySignature,
  createPublicKey,
  generateKeyPair,
  loadKeys
}